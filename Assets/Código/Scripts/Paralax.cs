﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour
{
    // Start is called before the first frame update
    Transform playerTransform;
    Vector2 oldPosition;
    public float paralexRatio;

    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        oldPosition = playerTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Repositon();
    }

    void Repositon(){
        if(playerTransform.position.y < oldPosition.y){
            transform.position = new Vector2(transform.position.x, transform.position.y + NewHeight());
        } else if(playerTransform.position.y > oldPosition.y){
            transform.position = new Vector2(transform.position.x, transform.position.y - NewHeight());
        }
        oldPosition = playerTransform.position;
    }

    float NewHeight(){
        return (playerTransform.position.y - oldPosition.x) * paralexRatio;
    }
}
