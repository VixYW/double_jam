﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New Game Data", menuName = "Game Data")]
public class GameData : ScriptableObject {

    static string skin;
    static int score;

    public int coins;
    public int gems;

    UnityEvent changeValue;

    public void AddCoins(int value) {
        coins += value;
        changeValue.Invoke();
    } 
    
    public void AddGems(int value) {
        gems += value;
        changeValue.Invoke();
    }

    public void Initialize() {
        changeValue = new UnityEvent();
    }

    public void AddListener(UnityAction action) {

        changeValue.AddListener(action);

    }

}
