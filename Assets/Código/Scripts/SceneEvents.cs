﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneEvents : MonoBehaviour {

    private static SceneEvents instance;
    public static SceneEvents current { get { return instance; } }

    private AsyncOperation operation;

    public Animator transitionAnimator;

    void Awake() {
        instance = this;
    }
 
    public void LoadScene(string sceneName) {

        transitionAnimator.Play("transitionGo");

        operation = SceneManager.LoadSceneAsync(sceneName);
        operation.allowSceneActivation = false;
    
    }

    public void EnableSceneLoad() {

        operation.allowSceneActivation = true;
    
    }

}
