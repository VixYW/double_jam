﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plsdontstopthemusic : MonoBehaviour
{
	static plsdontstopthemusic instance = null;
    
    void Awake()
    {
    	if (instance == null){
    		DontDestroyOnLoad(gameObject);
    		instance = this;
    	} else {
    		Destroy(gameObject);
    	}
    	
    }
}
