﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdMobController : MonoBehaviour {

    private RewardedAd rewardedAd;
    private int rewardId = 0;

    public static string adUnitId = "ca-app-pub-3940256099942544/5224354917";

    public GameData gameData;

    void Start() {

        rewardedAd = new RewardedAd(adUnitId);
        rewardedAd.OnUserEarnedReward += GetRewardByAd;

    }

    void GetRewardByAd(object sender, Reward args) {

        switch(rewardId) {

            case 0:

                Debug.Log("You got 100 coins");
                gameData.AddCoins(100);
                break;
            case 1:
                Debug.Log("You got 5 diamonds");
                //Ganha diamantes;
                break;

        }
    }

    public void CoinReward() {

        rewardId = 0;

        Debug.Log("Sucessful!");

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        rewardedAd.LoadAd(request);

    }   
    
    public void DiamondReward() {

        rewardId = 1;

        Debug.Log("Sucessful!");

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        rewardedAd.LoadAd(request);

    }

}
