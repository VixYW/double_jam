﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    private Player player;

    void Start(){
        player = GetComponent<Player>();
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), player.gameObject.GetComponent<Collider2D>());
    }

    void OnTriggerEnter2D(Collider2D col){
        Debug.Log(col.gameObject.tag);
        if (col.gameObject.tag == "Chão"){
            player.SetOnGround(true);
        }
    }
    
    void OnTriggerExit2D(Collider2D col){
        if (col.gameObject.tag == "Chão"){
            player.SetOnGround(false);
        }
    }
}
