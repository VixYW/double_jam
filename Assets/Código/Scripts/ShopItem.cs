﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MoneyType {

    Coin,
    Gems

}

public class ShopItem : MonoBehaviour
{
    public string itemName;
    public MoneyType moneyType;
    public int price;
    public bool skin;

    public GameObject previewButton;
    public GameObject useButton;
    public Button buyButton;

    bool sold = false;
    bool inUse = false;    

    [SerializeField]
    private GameData gameData;

    void Start() {

        int itemStatus = PlayerPrefs.GetInt(itemName, 0);
        if (itemStatus > 0){
            Sold();
            if (skin){
                if (PlayerPrefs.GetString("skin") == itemName){
                    inUse = true;
                }
            } else {
                if (itemStatus > 1){
                    inUse = true;
                }
            }
        }

    }

    public bool IsBuyable(){
        int budget = (moneyType == 0 ? gameData.coins : gameData.gems);
        if (budget >= price){
            buyButton.interactable = true;
            return true;
        } else {
            buyButton.interactable = false;
            return false;
        }
    }

    public void Buy(){
        if(moneyType == 0) {
            gameData.AddCoins(-price);
        } else {
            gameData.AddGems(-price);
        }
        Sold();
    }

    void Sold(){
        sold = true;
        previewButton.SetActive(false);
        useButton.SetActive(true);
    }

    public void Use(){
        if (skin){
            inUse = true;
            PlayerPrefs.SetString("skin", itemName);
            useButton.GetComponent<Button>().interactable = false;
        } else{
            if (inUse){
                inUse = false;
            } else {
                inUse = true;
            }
        }
    }

    void FixedUpdate(){
        if (skin && inUse){

        }
    }

}
