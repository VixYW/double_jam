﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestObject : MonoBehaviour
{
    // Start is called before the first frame update
    Player player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        //player.debug ? GetComponent<SpriteRenderer>().enabled = true: GetComponent<SpriteRenderer>().enabled = false;
        if (player.debug){
            GetComponent<SpriteRenderer>().enabled = true;
        } else{
            GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
