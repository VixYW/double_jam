﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour
{
    public Transform player;
    public GameObject start;
    public Button play;

    int intro;

    void Start(){
        if (PlayerPrefs.GetInt("firstTime", 0) == 0){
            intro = 0;
            //StartCoroutine(Intro());
        } else {
            intro = 1;
            transform.position = player.position;
        }
    }

    void Update()
    {
        switch(intro){
            case 1:
                transform.position = Vector2.MoveTowards(transform.position, player.position, 8);
                if (transform.position == player.position){
                    start.SetActive(true);
                    play.Select();
                    intro = 2;
                }
                break;
            case 2:
                transform.position = player.position;
                break;
        }
    }

    IEnumerator Intro(){
        yield return new WaitForSeconds(2);
        intro = 1;
    }

    public void Restart(){
        PlayerPrefs.SetInt("firstTime", 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
