﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour {

    [SerializeField]
    private GameData gameData;

    bool skinChange = false;

    public Text cashText;
    public Text gemsText;

    void Start() {

        gameData.Initialize();
        gameData.AddListener(UpdateValues);
        UpdateValues();

    }
    
    void UpdateValues(){
        cashText.text = gameData.coins.ToString();
        gemsText.text = gameData.gems.ToString();
    }

    /*
    public int GetCurrency(string currency)
    {
        switch (currency){
            case "cash":
                return cash;
            case "gems":
                return gems;
            default:
                Debug.LogError("Trying to get the value of " + currency + " but it doesn't exists.");
                return 0;
        }
    }

    public void SetCurrency(string currency, int value)
    {
        switch (currency){
            case "cash":
                cash = value;
                break;
            case "gems":
                gems = value;
                break;
            default:
                Debug.LogError("Trying to set the value of " + currency + " but it doesn't exists.");
                return;
        }
        PlayerPrefs.SetInt(currency, value);
        CurrencyUpdate();
    }
    */

    public bool GetSkinChange(){
        return skinChange;
    }

    public void SetSkinChange(bool change){
        skinChange = change;
    }
}
