﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public bool debug;

    [Header("Effect settings")]
    public SurfaceEffector2D track;
    public Camera cam;
    public ParticleSystem particles;
    
    [Header("Physics settings")]
    public float speedIncrease;
    public float fallMultiplier;
    public float jumpForce;
    public float groundCheckDistance = 1f;
    public int minimumSpeedJump;

    public Text speedText;
    public Text highScoreText;
    public Text scoreText;
    public GameObject restart;

    AudioSource audio;

    [Header("Audio settings")]
    public AudioClip Sifudeno;
    public AudioClip Comendo;

    [Header("Misc.")]
    public Button replay;
    public int ALTURA_DA_MORTE;

    Rigidbody2D playerBody;
    bool downwards;
    bool onGround;
    bool smackDown;
    bool running;
    int speed;
    int oldSpeed;
    int highestSpeed = 0;
    int score;
    DeviceType device;

    Vector2 oldPosition;
    Vector2 boostDirection;

    void Start(){
        oldPosition = transform.position;
    	Time.timeScale = 1;
        downwards = true;
        running = false;
        particles.Stop();
        playerBody = gameObject.GetComponent<Rigidbody2D>();
        highScoreText.text = "<b>Highscore: "+ PlayerPrefs.GetInt("highScore", 0).ToString()+"</b>";
        audio = GetComponent<AudioSource>();

        device = SystemInfo.deviceType;

    }

    void Update() {

        Velocimeter();
        CameraControl();

        scoreText.text = "Score: <b>" + score.ToString() + "</b>";
        if (downwards && running){ //
            //track.speed += speedIncrease;
            boostDirection = ((Vector2)transform.position - oldPosition).normalized;
            if (boostDirection.x > 0){
                playerBody.velocity += boostDirection * speedIncrease;
            }
            oldPosition = transform.position;
            //particles.emission.rateOverTime += speedIncrease;
        }

        if (device == DeviceType.Desktop){

            if (Input.GetKeyDown(KeyCode.UpArrow))
                Jump();

            if (Input.GetKeyDown(KeyCode.RightArrow) && downwards)
            {
                playerBody.velocity += Vector2.right;
            }
        } else if(device == DeviceType.Handheld) {

            if(Input.touchCount > 0) {

                if (Input.GetTouch(0).phase == 0)
                    Jump();

            }

        }



        if (speed <= 0 && !downwards){
            EndGame();
        }
        if (transform.position.y <= ALTURA_DA_MORTE){
            EndGame();
            Time.timeScale = 0;
        }

        GroundCheck();
        particles.emissionRate = speed;
    }

    void Jump() {
        if (speed >= minimumSpeedJump || downwards)
        {

            if (onGround)
            {

                playerBody.velocity += jumpForce * Vector2.up;
            }
            else if (!smackDown)
            {
                smackDown = true;
                playerBody.velocity += (playerBody.velocity.y * -1) * Vector2.up;
                if (speed < 20)
                {
                    playerBody.velocity += (jumpForce * 2) * Vector2.down;
                }
                else
                {
                    playerBody.velocity += (jumpForce * 6) * Vector2.down;
                }
                playerBody.velocity -= (jumpForce) * Vector2.right;
            }
        }
    }

    void GroundCheck() {

        RaycastHit2D[] raycast = Physics2D.RaycastAll(transform.position, Vector2.down, groundCheckDistance);
        
        if(debug)
            Debug.DrawLine(transform.position, transform.position + Vector3.down * (groundCheckDistance), Color.yellow);

        foreach(RaycastHit2D r in raycast) {

            if (r.transform.gameObject.tag == "Chão") {
                SetOnGround(true);
                return;
            }
        }

        SetOnGround(false);

    }

    void FixedUpdate(){
        if (playerBody.velocity.y < -0.6 && !onGround){
            playerBody.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - playerBody.gravityScale) * Time.deltaTime;
        }
    }

    void Velocimeter(){
        oldSpeed = speed;
        if (downwards){
            speed = (int) Math.Abs(((Math.Abs(playerBody.velocity.x) + Math.Abs(playerBody.velocity.y / 2))));
        } else {
            speed = (int) Math.Abs(playerBody.velocity.x);
        }
         //(int) Math.Abs(playerBody.velocity.x)
        speedText.text = "Speed: <b>" + speed.ToString() + "</b> <size=18>Km/h</size>";
        if(speed > highestSpeed){
            highestSpeed = speed;
        }
    }

    void CameraControl(){
        Vector3 nextCameraPoint = new Vector3(cam.transform.position.x, cam.transform.position.y, (highestSpeed*-1)/3 - 10);
        cam.transform.position = Vector3.Lerp(cam.transform.position, nextCameraPoint, 0.05f);
    }

    void OnTriggerEnter2D(Collider2D col){
        if (col.tag == "Flag"){
            downwards = false;
            particles.Stop();
        }
        if (col.tag == "Candy"){
            score += col.GetComponent<Candy>().value;
        	audio.clip = Comendo;
        	audio.Play();
            Destroy(col.gameObject);
        }
        if (col.tag == "BUCETA"){
            score += col.GetComponent<Candy>().value;
            audio.clip = Sifudeno;
        	audio.Play();
            Destroy(col.gameObject);
        }
    }

    public void SetOnGround(bool setter){
        if (setter){
            smackDown = false;
        }
        onGround = setter;
    }

    void EndGame(){
        playerBody.velocity = new Vector2(0,0);
        if (score > PlayerPrefs.GetInt("highScore", 0)){
            PlayerPrefs.SetInt("highScore", score);
            highScoreText.text = "New <b>Highscore: "+PlayerPrefs.GetInt("highScore", 0).ToString()+"</b>";
        }
        //PlayerPrefs.SetInt("cash", PlayerPrefs.GetInt("cash", 0) + score);
        restart.SetActive(true);
        replay.Select();
    }

    public void SetRunning(){
        running = true;
    }
}
